package ru.omsu;

import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static ru.omsu.Demo.main;

public class testSort {

    public final String STRING_PATH = "D:\\files\\string";
    public final String INT_PATH = "D:\\files\\integer";
    public final String CORRECT_FILES_PATH = "D:\\files\\right";

    @Test
    public void TestOK() throws ParameterException {
        String[] arr = new String[4];
        arr[0] = STRING_PATH;
        arr[1] = "sorted_";
        arr[2] = "s";
        arr[3] = "d";

        main(arr);

    }

    @Test
    public void TestSortIntAscending() throws FileOperationsExeption, ParameterException {
        String[] arr = new String[4];
        arr[0] = INT_PATH;
        arr[1] = "sorted_";
        arr[2] = "i";
        arr[3] = "a";

        main(arr);

        File dir = new File(arr[0]);
        List<File> listFiles = Arrays.asList(dir.listFiles());

        File dirR = new File(CORRECT_FILES_PATH);
        List<File> listFilesR = Arrays.asList(dirR.listFiles());

        List<String> exp1 = new FileOperations().readFromFile(listFiles.get(0));
        List<String> test1 = new FileOperations().readFromFile(listFilesR.get(0));

        List<String> exp2 = new FileOperations().readFromFile(listFiles.get(1));
        List<String> test2 = new FileOperations().readFromFile(listFilesR.get(1));

        List<String> exp3 = new FileOperations().readFromFile(listFiles.get(2));
        List<String> test3 = new FileOperations().readFromFile(listFilesR.get(2));

        assertEquals(test1, exp1);
        assertEquals(test2, exp2);
        assertEquals(test3, exp3);
    }

    @Test
    public void TestSortIntDecrease() throws FileOperationsExeption, ParameterException {
        String[] arr = new String[4];
        arr[0] = INT_PATH;
        arr[1] = "sorted_";
        arr[2] = "i";
        arr[3] = "d";

        main(arr);


        File dir = new File(arr[0]);
        List<File> listFiles = Arrays.asList(dir.listFiles());

        File dirR = new File(CORRECT_FILES_PATH);
        List<File> listFilesR = Arrays.asList(dirR.listFiles());

        List<String> exp1 = new FileOperations().readFromFile(listFiles.get(0));
        List<String> test1 = new FileOperations().readFromFile(listFilesR.get(3));

        List<String> exp2 = new FileOperations().readFromFile(listFiles.get(1));
        List<String> test2 = new FileOperations().readFromFile(listFilesR.get(4));

        List<String> exp3 = new FileOperations().readFromFile(listFiles.get(2));
        List<String> test3 = new FileOperations().readFromFile(listFilesR.get(5));

        assertEquals(test1, exp1);
        assertEquals(test2, exp2);
        assertEquals(test3, exp3);

    }

    @Test
    public void TestSortStrAscending() throws FileOperationsExeption, ParameterException {
        String[] arr = new String[4];
        arr[0] = STRING_PATH;
        arr[1] = "sorted_";
        arr[2] = "s";
        arr[3] = "a";

        main(arr);

        File dir = new File(arr[0]);
        List<File> listFiles = Arrays.asList(dir.listFiles());

        File dirR = new File(CORRECT_FILES_PATH);
        List<File> listFilesR = Arrays.asList(dirR.listFiles());

        List<String> exp1 = new FileOperations().readFromFile(listFiles.get(0));
        List<String> test1 = new FileOperations().readFromFile(listFilesR.get(6));

        List<String> exp2 = new FileOperations().readFromFile(listFiles.get(1));
        List<String> test2 = new FileOperations().readFromFile(listFilesR.get(7));

        List<String> exp3 = new FileOperations().readFromFile(listFiles.get(2));
        List<String> test3 = new FileOperations().readFromFile(listFilesR.get(8));

        assertEquals(test1, exp1);
        assertEquals(test2, exp2);
        assertEquals(test3, exp3);
    }


    @Test
    public void TestSortStrDecrease() throws FileOperationsExeption, ParameterException {
        String[] arr = new String[4];
        arr[0] = STRING_PATH;
        arr[1] = "sorted_";
        arr[2] = "s";
        arr[3] = "d";

        main(arr);

        File dir = new File(arr[0]);
        List<File> listFiles = Arrays.asList(dir.listFiles());

        File dirR = new File(CORRECT_FILES_PATH);
        List<File> listFilesR = Arrays.asList(dirR.listFiles());

        List<String> exp1 = new FileOperations().readFromFile(listFiles.get(0));
        List<String> test1 = new FileOperations().readFromFile(listFilesR.get(9));

        List<String> exp2 = new FileOperations().readFromFile(listFiles.get(1));
        List<String> test2 = new FileOperations().readFromFile(listFilesR.get(10));

        List<String> exp3 = new FileOperations().readFromFile(listFiles.get(2));
        List<String> test3 = new FileOperations().readFromFile(listFilesR.get(11));

        assertEquals(test1, exp1);
        assertEquals(test2, exp2);
        assertEquals(test3, exp3);
    }

    @Test(expected = ParameterException.class)
    public void TestFewParam() throws ParameterException {
        String[] arr = new String[3];
        arr[0] = STRING_PATH;
        arr[1] = "sorted_";
        arr[2] = "s";
        main(arr);
    }

    @Test(expected = ParameterException.class)
    public void TestManyParam() throws ParameterException {
        String[] arr = new String[5];
        arr[0] = STRING_PATH;
        arr[1] = "sorted_";
        arr[2] = "s";
        arr[3] = "s";
        arr[4] = "s";
        main(arr);
    }

    @Test(expected = ParameterException.class)
    public void TestWrongDir() throws ParameterException {
        String[] arr = new String[4];
        arr[0] = "D:\\filess";
        arr[1] = "sorted_";
        arr[2] = "s";
        arr[3] = "a";

        main(arr);
    }

    @Test(expected = ParameterException.class)
    public void TestWrongSortMode() throws ParameterException {
        String[] arr = new String[4];
        arr[0] = STRING_PATH;
        arr[1] = "sorted_";
        arr[2] = "s";
        arr[3] = "r";

        main(arr);
    }

    @Test(expected = ParameterException.class)
    public void TestWrongContentType() throws ParameterException {
        String[] arr = new String[4];
        arr[0] = STRING_PATH;
        arr[1] = "sorted_";
        arr[2] = "t";
        arr[3] = "a";

        main(arr);
    }
}
