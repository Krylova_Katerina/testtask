package ru.omsu;

enum ErrorCodes {

    FILE_NOT_FOUND("File/directory not found "),
    WRONG_DATA_INT("This isn't a number / contains a character"),
    WRONG_CONTENT_TYPE("Wrong content type"),
    WRONG_SORT_MODE("Wrong sort mode"),
    FEW_PARAM("Few parameters. Add parameters"),
    MANY_PARAM("Many parameters. Take it away");

    private String message;

    private ErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}


