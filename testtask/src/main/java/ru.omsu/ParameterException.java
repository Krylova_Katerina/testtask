package ru.omsu;

public class ParameterException extends Exception {

    public ParameterException(Throwable cause, ErrorCodes message, String param) {
        super(String.format(message.getMessage(), param), cause);
    }

    public ParameterException(ErrorCodes message, String param) {
        super(String.format(message.getMessage(), param));
    }

    public ParameterException(ErrorCodes message) {
        super(message.getMessage());
    }


}
