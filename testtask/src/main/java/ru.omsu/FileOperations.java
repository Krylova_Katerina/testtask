package ru.omsu;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileOperations {
    private String pathDir;
    private String filePrefix;
    private File fileName;

    public FileOperations(String pathDir, String filePrefix, File fileName) {
        this.pathDir = pathDir;
        this.filePrefix = filePrefix;
        this.fileName = fileName;
    }


    public FileOperations() {
    }

    public String newFileName(String pathDir, String filePrefix, File fileName) {
        return pathDir + '/' + filePrefix + fileName.getName();
    }

    public void writeToFile(File fileName, String filePrefix, String pathDir, List<?> data) throws FileOperationsExeption {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(newFileName(pathDir, filePrefix, fileName)))) {
            for (int i = 0; i < data.size(); i++) {
                bw.write(String.valueOf(data.get(i)));
                bw.newLine();
            }
        } catch (FileNotFoundException e) {
            throw new FileOperationsExeption(e, ErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsExeption(e);
        }
    }

    public List<String> readFromFile(File fileName) throws FileOperationsExeption {
        List<String> result = new ArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String str;
            str = br.readLine();
            while (str != null) {
                result.add(str);
                str = br.readLine();
            }
        } catch (FileNotFoundException e) {
            throw new FileOperationsExeption(e, ErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsExeption(e);
        }
        return result;
    }

    public String getPathDir() {
        return pathDir;
    }

    public String getFilePrefix() {
        return filePrefix;
    }

    public File getFileName() {
        return fileName;
    }
}
