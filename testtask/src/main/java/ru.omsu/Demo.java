package ru.omsu;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Demo {

    public static void main(String[] arr) throws ParameterException {
/**
 * java –jar sort-it.jar C:\in_dir\ --out-prefix=sorted_ --content-type=i --sort-mode=a
 *  0 C:\in_dir\ Директория с файлами
 *  1 out-prefix=sorted_ префикс для выходных файлов (добавление к началу имени файла)
 *  2 content-type=i тип содержимого (i -- int, s --string )
 *  3 sort-mode=a режим сортировки (d -- decrease -- убывание, a -- ascending -- возрастание)
 **/

        if (arr.length < 4) {
            throw new ParameterException(ErrorCodes.FEW_PARAM);
        } else if (arr.length > 4) {
            throw new ParameterException(ErrorCodes.MANY_PARAM);
        } else if (arr.length == 4) {

            File dir = new File(arr[0]);
            if (!dir.exists()) {
                throw new ParameterException(ErrorCodes.FILE_NOT_FOUND);
            }
            File[] arrFiles = dir.listFiles();
            List<File> listFiles = Arrays.asList(arrFiles);

            for (int i = 0; i < listFiles.size(); i++) {
                Sorting sort = new Sorting(arr[2], arr[3], new FileOperations(arr[0], arr[1], listFiles.get(i)));
                sort.run();
            }
        }

    }


}

