package ru.omsu;

public class FileOperationsExeption extends Exception {

    public FileOperationsExeption(Throwable cause, ErrorCodes message) {
        super(message.getMessage(), cause);
    }

    public FileOperationsExeption(Throwable cause) {
        super(cause);
    }
}
