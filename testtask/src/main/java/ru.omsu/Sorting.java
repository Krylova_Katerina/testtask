package ru.omsu;

import java.util.ArrayList;
import java.util.List;

public class Sorting extends Thread {

    private String contentType;
    private String sortMode;
    private FileOperations fileOperations;

    public Sorting(String contentType, String sortMode, FileOperations fileOperations) {
        this.contentType = contentType;
        this.sortMode = sortMode;
        this.fileOperations = fileOperations;
    }

    public List<Integer> convertToInt(List<String> str) throws ParameterException {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < str.size(); i++) {
            try {
                result.add(Integer.valueOf(str.get(i)));
            } catch (NumberFormatException e) {
                throw new ParameterException(e, ErrorCodes.WRONG_DATA_INT, str.get(i));
            }
        }
        return result;
    }

    public void doSortStr(List<String> data, String sortMode) throws FileOperationsExeption, ParameterException {
        String temp;
        int j;
        int size = data.size();
        if (sortMode.equals("a")) {
            for (int i = 0; i < size - 1; i++) {
                if (data.get(i).compareTo(data.get(i + 1)) > 0) {
                    temp = data.get(i + 1);
                    data.set(i + 1, data.get(i));
                    j = i;
                    while (j > 0 && temp.compareTo(data.get(j - 1)) < 0) {
                        data.set(j, data.get(j - 1));
                        j--;
                    }
                    data.set(j, temp);
                }
            }
        } else if (sortMode.equals("d")) {
            for (int i = 0; i < size - 1; i++) {
                if (data.get(i).compareTo(data.get(i + 1)) < 0) {
                    temp = data.get(i + 1);
                    data.set(i + 1, data.get(i));
                    j = i;
                    while (j > 0 && temp.compareTo(data.get(j - 1)) > 0) {
                        data.set(j, data.get(j - 1));
                        j--;
                    }
                    data.set(j, temp);
                }
            }
        } else {
            throw new ParameterException(ErrorCodes.WRONG_SORT_MODE, sortMode);
        }
        fileOperations.writeToFile(fileOperations.getFileName(), fileOperations.getFilePrefix(), fileOperations.getPathDir(), data);
    }

    public void doSortInt(List<Integer> data, String sortMode) throws FileOperationsExeption, ParameterException {
        int temp, j;
        int size = data.size();
        if (sortMode.equals("d")) {
            for (int i = 0; i < size - 1; i++) {
                if (data.get(i) < data.get(i + 1)) {
                    temp = data.get(i + 1);
                    data.set(i + 1, data.get(i));
                    j = i;
                    while (j > 0 && temp > data.get(j - 1)) {
                        data.set(j, data.get(j - 1));
                        j--;
                    }
                    data.set(j, temp);
                }
            }
        } else if (sortMode.equals("a")) {
            for (int i = 0; i < size - 1; i++) {
                if (data.get(i) > data.get(i + 1)) {
                    temp = data.get(i + 1);
                    data.set(i + 1, data.get(i));
                    j = i;
                    while (j > 0 && temp < data.get(j - 1)) {

                        data.set(j, data.get(j - 1));
                        j--;
                    }
                    data.set(j, temp);
                }
            }
        } else {
            throw new ParameterException(ErrorCodes.WRONG_SORT_MODE, sortMode);
        }
        fileOperations.writeToFile(fileOperations.getFileName(), fileOperations.getFilePrefix(), fileOperations.getPathDir(), data);
    }

    public void doSort(String contentType, String sortMode, List<String> data) throws ParameterException, FileOperationsExeption {
        if (contentType.equals("i")) {
            doSortInt(convertToInt(data), sortMode);
        } else if (contentType.equals("s")) {
            doSortStr(data, sortMode);
        } else {
            throw new ParameterException(ErrorCodes.WRONG_CONTENT_TYPE, contentType);
        }
    }

    @Override
    public void run() {
        try {
            doSort(contentType, sortMode, fileOperations.readFromFile(fileOperations.getFileName()));
        } catch (FileOperationsExeption | ParameterException ex) {
            ex.printStackTrace();
        }

    }
}